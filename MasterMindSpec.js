class MasterMind {
    constructor(passwordGoal) {
        this.passwordGoal = passwordGoal
    }

    compare(passwordTest) {
        let pins = {wellPlaced: 0, misplaced: 0}
        this.passwordGoal.forEach((colour, index) => {
            if (colour === passwordTest[index]) {
                pins.wellPlaced += 1
            }
        });
        return pins
    }
}

describe("Master Mind", () => {
    it("returns no pin when every colour is wrong", () => {
        // Arrange
        const passwordGoal = ['GREEN', 'GREEN', 'GREEN', 'GREEN']
        const passwordTest = ['RED', 'RED', 'RED', 'RED']
        const game = new MasterMind(passwordGoal)

        // Act
        const pins = game.compare(passwordTest)

        // Assert
        expect(pins).toBe({wellPlaced: 0, misplaced: 0})
    })

    it("returns full well placed pins when every colour is correct", () => {
        const passwordGoal = ['GREEN', 'GREEN', 'GREEN', 'GREEN']
        const passwordTest = ['GREEN', 'GREEN', 'GREEN', 'GREEN']
        const game = new MasterMind(passwordGoal)

        const pins = game.compare(passwordTest)

        expect(pins).toBe({wellPlaced: 4, misplaced: 0})
    })

    it("returns full well placed pins when every colour is correct", () => {
        const passwordGoal = ['BLUE', 'BLUE', 'BLUE', 'BLUE']
        const passwordTest = ['BLUE', 'BLUE', 'BLUE', 'BLUE']
        const game = new MasterMind(passwordGoal)

        const pins = game.compare(passwordTest)

        expect(pins).toBe({wellPlaced: 4, misplaced: 0})
    })

    it("some colours are well placed", () => {
        const passwordGoal = ['BLUE', 'BLUE', 'BLUE', 'BLUE']
        const passwordTest = ['BLUE', 'BLUE', 'BLUE', 'GREEN']
        const game = new MasterMind(passwordGoal)

        const pins = game.compare(passwordTest)

        expect(pins).toBe({wellPlaced: 3, misplaced: 0})
    })
})